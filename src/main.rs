// The following can be changed -- try it out
const HEIGHT: f64 = 180.0;
const WIDTH:  f64 = 500.0;
const FONT:  &str = "Terminus 35";
const TEXT:  &str = "👉😎👉Z̵̘̮̳̟̼̺͉̰̩̝̃̈̈́̈́̌̆́̓̋͘͝ͅǫ̴̨̛̩̠̼̼̜̽̀͒̾̔̌̿͐̈́̐̔̄͊͝ò̵̮͎̳̝̭̲̞̼̬̪͒̊́̎̈́͛́͆̌p̵̭̟̖̗͐̽̽͆̎̏͒͋̅̐̾͌ẹ̶͌͐̃̓̾̊̏́d̴͚̰̬̾̏͛̍̑̃̚̚👉😎👉";

/// Utility function: used when setting up xcb
fn get_root_visual_type(screen: &xcb::Screen) -> xcb::Visualtype {
    screen.allowed_depths()
	.flat_map(|depth| depth.visuals())
	.find(|visual| screen.root_visual() == visual.visual_id())
	.expect("No visual type found")
}

/// Create cairo context for drawing, links to xcb here
fn create_cairo_context(conn: &xcb::Connection,
                        screen: &xcb::Screen,
                        window: &xcb::Window)
                        -> cairo::Context {
    let surface;
    unsafe {
        let cairo_conn = cairo::XCBConnection::from_raw_none(conn.get_raw_conn() as
                                                             *mut cairo_sys::xcb_connection_t);
        let visual_ptr: *mut cairo_sys::xcb_visualtype_t =
            &mut get_root_visual_type(&screen).base as *mut _ as *mut cairo_sys::xcb_visualtype_t;
        let visual = cairo::XCBVisualType::from_raw_none(visual_ptr);
        let cairo_screen = cairo::XCBDrawable(window.to_owned());
        surface = cairo::XCBSurface::create(&cairo_conn, &cairo_screen, &visual, WIDTH as i32, HEIGHT as i32).unwrap();
    }

    cairo::Context::new(&surface)
}

/// Create a pango layout, used for drawing text, links to cairo
fn create_pango_layout(cr: &cairo::Context) -> pango::Layout {
    let layout = pangocairo::create_layout(&cr).unwrap();
    layout.set_font_description(Some(&pango::FontDescription::from_string(FONT)));
    layout
}

/// Creates and initialized an xcb window, returns (screen, window)
fn create_xcb_window<'a>(conn: &'a xcb::Connection, screen_num: i32) -> (xcb::StructPtr<'a, xcb::ffi::xcb_screen_t>, u32) {
    let screen =
	conn.get_setup().roots().nth(screen_num as usize).unwrap();
    let window = conn.generate_id();

    xcb::create_window(&conn,
		       xcb::COPY_FROM_PARENT as u8,
		       window,
		       screen.root(),
		       20, 20,
		       WIDTH as u16, HEIGHT as u16,
		       0,
		       xcb::WINDOW_CLASS_INPUT_OUTPUT as u16,
		       screen.root_visual(),
		       &[(xcb::CW_EVENT_MASK,
                          xcb::EVENT_MASK_EXPOSURE | xcb::EVENT_MASK_KEY_PRESS),
                         (xcb::CW_OVERRIDE_REDIRECT, 1)
		       ]);

    xcb::map_window(&conn, window);

    (screen, window)
}

/// sets up xkb
fn setup_xkb(conn: &xcb::Connection, window: xcb::Window) -> xkbcommon::xkb::State {

    use xcb::xkb;
    
    conn.prefetch_extension_data(xkb::id());

    // generally useful to retrieve the first event from this
    // extension. event response_type will be set on this
    let _first_ev = match conn.get_extension_data(xkb::id()) {
        Some(r) => r.first_event(),
        None => { panic!("XKB extension not supported by X server!"); }
    };


    // we need at least xcb-xkb-1.0 to be available on client
    // machine
    let cookie = xkb::use_extension(&conn, 1, 0);

    match cookie.get_reply() {
        Ok(r) => {
            if !r.supported() {
                panic!("xkb-1.0 is not supported");
            }
        },
        Err(_) => {
            panic!("could not get xkb extension supported version");
        }
    };

    // we now select what events we want to receive
    // such as map change, keyboard hotplug ...
    // note that key strokes are given directly by
    // the XCB_KEY_PRESS event from xproto, not by xkb
    let map_parts =
        xkb::MAP_PART_KEY_TYPES |
    xkb::MAP_PART_KEY_SYMS |
    xkb::MAP_PART_MODIFIER_MAP |
    xkb::MAP_PART_EXPLICIT_COMPONENTS |
    xkb::MAP_PART_KEY_ACTIONS |
    xkb::MAP_PART_KEY_BEHAVIORS |
    xkb::MAP_PART_VIRTUAL_MODS |
    xkb::MAP_PART_VIRTUAL_MOD_MAP;

    let events =
        xkb::EVENT_TYPE_NEW_KEYBOARD_NOTIFY |
    xkb::EVENT_TYPE_MAP_NOTIFY |
    xkb::EVENT_TYPE_STATE_NOTIFY;

    let cookie = xkb::select_events_checked(&conn,
					    xkb::ID_USE_CORE_KBD as u16,
					    events as u16, 0, events as u16,
					    map_parts as u16, map_parts as u16, None);

    cookie.request_check().expect("failed to select notify events from xcb xkb");

    // grab keyboard -- needed because of redirect override
    let cookie = xcb::grab_keyboard(&conn, true, window, xcb::CURRENT_TIME, xcb::GRAB_MODE_ASYNC as u8, xcb::GRAB_MODE_ASYNC as u8);

    assert!(cookie.get_reply().expect("failed to get reply while grabbing keyboard").status() as u32
	    == xcb::GRAB_STATUS_SUCCESS, "failed to grab keyboard: invalid something");

    reload_xkb_map(conn)
}

/// utility function -- re/load the keymap
fn reload_xkb_map(conn: &xcb::Connection) -> xkbcommon::xkb::State {
    let context = xkbcommon::xkb::Context::new(xkbcommon::xkb::CONTEXT_NO_FLAGS);
    let id = xkbcommon::xkb::x11::get_core_keyboard_device_id(conn);
    let keymap = xkbcommon::xkb::x11::keymap_new_from_device(&context, conn, id, xkbcommon::xkb::KEYMAP_COMPILE_NO_FLAGS);
    xkbcommon::xkb::x11::state_new_from_device(&keymap, conn, id)
}

/// Entry point
fn main() {
    println!("Press 'q' to exit. Keystrokes will be printed below:");
    
    // xcb init
    let (conn, screen_num) = xcb::Connection::connect(None).unwrap();
    let (screen, window) = create_xcb_window(&conn, screen_num);
    // xkb init
    let mut xkb_state = setup_xkb(&conn, window);
    
    //cairo init
    let cr = create_cairo_context(&conn, &screen, &window);
    //pango init
    let layout = create_pango_layout(&cr);

    // The following waits for X to expose the window, then paints some stuff on it
    // After that, it waits for keypresses, and prints them
    loop {
        let event = conn.wait_for_event();
        match event {
            None => {
                break;
            },
            Some(event) => {
                let r = event.response_type() & !0x80;
                match r {
                    xcb::EXPOSE => {
			// background, gets drawn over, leaving a gray triangle
                        cr.set_source_rgb(0.5, 0.5, 0.5);
                        cr.paint();

			// red triangle
                        cr.set_source_rgb(1.0, 0.0, 0.0);
                        cr.move_to(0.0, 0.0);
                        cr.line_to(WIDTH, 0.0);
                        cr.line_to(WIDTH, HEIGHT);
                        cr.close_path();
                        cr.fill();

			// blue center line
                        cr.set_source_rgb(0.0, 0.0, 1.0);
                        cr.set_line_width(20.0);
                        cr.move_to(0.0, 0.0);
                        cr.line_to(WIDTH, HEIGHT);
                        cr.stroke();

			// get ready to draw text
			layout.set_text(TEXT);
			// get a size hint for allignment
			let (mut text_width, mut text_height) = layout.get_size();
			// If the text is too wide, ellipsize to fit
			if text_width > WIDTH as i32*pango::SCALE {
			    text_width = WIDTH as i32*pango::SCALE;
			    layout.set_ellipsize(pango::EllipsizeMode::End);
			    layout.set_width(text_width);
			}
			// scale back -- pango doesn't uses large integers instead of floats
			text_width /= pango::SCALE;
			text_height /= pango::SCALE;
			// base text color (does not apply to color bitmap chars)
			cr.set_source_rgb(1.0, 1.0, 1.0);
			// place and draw text
			cr.move_to((WIDTH-text_width as f64)/2.0, (HEIGHT-text_height as f64)/2.0);
			pangocairo::show_layout(&cr, &layout);

			// wait for everything to finish drawing before moving on
			conn.flush();
                    },
                    xcb::KEY_PRESS => {
			
			// Use xkb to convert from key event to key name, the print
                        let key_press: &xcb::KeyPressEvent = unsafe{xcb::cast_event(&event)};
			let keycode = key_press.detail().into();
			
			let sym = xkb_state.key_get_one_sym(keycode);

			let name = xkbcommon::xkb::keysym_get_name(sym);

			if xkb_state.mod_name_is_active(xkbcommon::xkb::MOD_NAME_CTRL, xkbcommon::xkb::STATE_MODS_EFFECTIVE) {
			    print!("C-");
			}
			if xkb_state.mod_name_is_active(xkbcommon::xkb::MOD_NAME_ALT,  xkbcommon::xkb::STATE_MODS_EFFECTIVE) {
			    print!("M-");
			}
			if xkb_state.mod_name_is_active(xkbcommon::xkb::MOD_NAME_LOGO, xkbcommon::xkb::STATE_MODS_EFFECTIVE) {
			    print!("s-");
			}
                        println!("{}", name);
			if name == "q" {
			    break;
			}
			
			xkb_state.update_key(keycode, xkbcommon::xkb::KeyDirection::Down);
                    },
		    xcb::KEY_RELEASE => {
                        let key_press: &xcb::KeyPressEvent = unsafe{xcb::cast_event(&event)};
			let keycode = key_press.detail().into();
			xkb_state.update_key(keycode, xkbcommon::xkb::KeyDirection::Up);
		    },
		    xcb::MAP_NOTIFY => {
			xkb_state = reload_xkb_map(&conn);
		    },
                    _ => {}
                }
            }
        }
    }
}
